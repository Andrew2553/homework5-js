// 1. Метод объекта является функцией, которая связана с определенным объектом и выполняет действия, связанные с этим объектом. Он выполняется в контексте объекта и имеет доступ к его свойствам и другим методам. Методы используются для реализации поведения объекта, они выполняют операции, обрабатывают данные или обеспечивают другие функциональные возможности, связанные с объектом.

// 2. Значение свойства объекта в JavaScript может иметь любой тип данных, доступный в языке. Основные типы данных, которые могут быть значениями свойств объекта, включают:

// Примитивные типы:

// Строки
// Числа 
// Булевые 
// null
// undefined

// Объекты:

// Объекты 
// Массивы 
// Функции 
// Регулярные выражения 

// Специальные типы:

// Объекты даты (date objects): new Date()
// Объекты ошибок (error objects): new Error("Ошибка!")

// 3. Це означає, що коли ми присвоюємо об'єкт одній змінній і потім присвоюємо цю змінну іншій змінній або передаємо її в функцію, то обидві змінні будуть посилатись на один і той самий об'єкт в пам'яті.




function createNewUser() {
    const newUser = {
      firstName: prompt("Введіть ваше ім'я:"),
      lastName: prompt("Введіть ваше прізвище:")
    };
  
    newUser.getLogin = function() {
      const login = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
      return login;
    };
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());
  